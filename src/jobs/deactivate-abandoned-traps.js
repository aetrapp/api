const config = require('config');
const moment = require('moment');

// Sequelize
const Sequelize = require("sequelize");
const { Op } = Sequelize;

// Logger
const logger = require('winston');

/*
 * Find traps delayed traps and update their statuses
 */
module.exports = async app => {

  const maxCycleDuration = config.get("maxCycleDuration");
  const traps = app.service("traps");
  const notifications = app.service("notifications");

  const cycleStartLimit = moment().subtract(maxCycleDuration, 'days').toDate();

  const delayedTraps = await traps.find({
    skipResolver: true,
    query: {
      cycleStart: {
        [Op.lt]: cycleStartLimit
      },
      status: {
        [Op.ne]: "inactive"
      }
    },
    paginate: false
  });

  if (delayedTraps && delayedTraps.length > 0) {
    delayedTraps.forEach(async trap => {
      try {
        // Deactivate trap
        await traps.patch(trap.id, { status: "inactive" }, { updateStatus: true });

        // Create notification
        await notifications.create({
          recipientId: trap.ownerId,
          payload: {
            type: 'automatic-trap-deactivation',
            deeplink: 'trap/' + trap.id,
            trapId: trap.id
          },
          title: "Desativação automática de armadilha",
          message: `A armadilha ${trap.id} está há mais de ${maxCycleDuration} dias sem manutenção no sistema e foi desativada automaticamente. Descarte a armadilha e, caso queira continuar a coletar ovos, inicie um novo ciclo.`
        });

      } catch (error) {
        logger.error(`Error deactivating abandoned trap (${trap.id}): ${error.message}`);
      }
    });
  }
};
