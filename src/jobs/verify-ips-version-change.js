const axios = require('axios');
const sequelize = require('sequelize');

// Logger
const logger = require('winston');

// Sequelize operators
const Sequelize = require("sequelize");
const { Op } = Sequelize;

/*
 * If a newer IPS version is available, update sample counts by running new analysis
 */
module.exports = async app => {
  const ipsUrl = app.get("ipsUrl");
  let res = await axios
    .get(ipsUrl + "/api/description")
    .catch(err => {
      logger.error(`Update sample count error: ${err.message}`);
      return;
    });

  if (res && res.data) {
    const { ipsVersion } = res.data;
    const samples = app.service("samples");
    const queryResult = await samples.find({
      skipResolver: true,
      sequelize: {
        order: sequelize.literal('random()')
      },
      query: {
        status: {
          [Op.ne]: 'analysing'
        },
        "ipsData.ipsVersion": {
          [Op.ne]: ipsVersion
        }
      },
      $limit: 1
    }).catch (err => {
      logger.error(`Update sample count error: ${err.message}`);
      return;
    });

    // A sample was found
    if (queryResult && queryResult.data && queryResult.data.length > 0) {
      logger.info(`Analysis for sample (${sample.id}) is outdated, re-analysing...`);
      const sample = queryResult.data[0];
      res = await samples.patch(sample.id, {
        status: "analysing"
      }).catch(err => {
        logger.error(`Update sample count error: ${err.message}`);
        return;
      });
    }
  }
};
