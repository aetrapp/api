const Sequelize = require('sequelize');
const logger = require('winston');

module.exports = function () {
  const app = this;

  app.get("/summary", async function (req, res) {

    // Sequelize Client and Operator
    const sequelize = app.get('sequelizeClient');
    const { ne } = Sequelize.Op;

    const activeTraps = await app.service('traps').find({
      query: {
        $limit: 0,
        status: { [ne]: 'inactive' }
      }
    });

    const registeredTraps = await app.service('traps').find({
      query: {
        $limit: 0
      }
    });

    const activeUsers = await app.service('users').find({
      query: {
        $limit: 0,
        isActive: true
      }
    });

    let eggCount = 0;
    const count = await sequelize.query('select sum("eggCount") from samples;', { type: sequelize.QueryTypes.SELECT });
    try {
      eggCount = Number(count[0].sum);
    } catch (error) {
      logger.error('Error calculating sample count for summary:', error.message);
    }

    return res.json({
      activeTraps: activeTraps.total,
      registeredTraps: registeredTraps.total,
      activeUsers: activeUsers.total,
      eggCount
    });

  });
};
