const moment = require("moment");

// Sequelize
const Sequelize = require("sequelize");
const { Op } = Sequelize;

module.exports = () => async context => {

  if (!context.id) return context;

  const trap = {
    ...await context.service.get(context.id),
    ...context.data
  };

  const allowedManualStatus = ["inactive"];

  // valid status:
  //   "waiting-sample"
  //   "no-eggs"
  //   "above-average"
  //   "below-average"
  //   "delayed"
  //   "inactive"

  if (context.params.updateStatus) {
    // Allowed manual status
    if(allowedManualStatus.indexOf(context.data.status) !== -1) {
      if (context.data.status == 'inactive') {
        context.data.cycleIsFinished = false;
        context.data.isActive = false;
      }
      return context;
    }

    // Delayed
    const delayedDate = moment().subtract(trap.cycleDuration, 'days').toDate();
    if(trap.cycleStart < delayedDate) {
      context.data.status = "delayed";
      return context;
    }

    // Comparison statuses (no-eggs, above average, below average or waiting sample)

    const citiesService = context.app.service("cities");
    const samplesService = context.app.service("samples");
    const aWeekAgo = moment().subtract(7, 'days').toDate();

    // check if a valid sample exists in last week
    let recentValidSample = await samplesService.find({
      query: {
        trapId: trap.id,
        collectedAt: {
          [Op.gte]: aWeekAgo
        },
        status: 'valid',
        $limit: 1,
        $sort: {
          collectedAt: -1
        }
      },
      paginate: false,
      skipResolver: true
    });

    const city = await citiesService.get(trap.cityId, { skipResolver: true });

    let status;
    // if there is a recent sample
    if (recentValidSample && recentValidSample.length > 0 && city.eggCountAverageDate) {
      recentValidSample = recentValidSample[0];

      // determine status
      if (recentValidSample.eggCount == 0) {
        status = 'no-eggs';
        // if city has recent count, set status as comparison
      } else if (trap.relationToCityAverage) {
        status = trap.relationToCityAverage;
      } else {
        status = 'waiting-sample';
      }
    } else {
      status = 'waiting-sample';
    }
    context.data.status = status;
    return context;

  }
  return context;
};
