const { getItems } = require("feathers-hooks-common");
const config = require('config');

module.exports = () => async context => {
  const trap = getItems(context);

  if (Array.isArray(trap)) {
    return context;
  }

  // The webapp is sending "isActive=false" parameter to deactivate traps.
  // Maybe "isActive" property should be removed from model, until then, pass status='inactive'.
  if (context.data.isActive == false) {
    context.data.status = 'inactive';
  }

  if (context.data.restartCycle || context.data.isActive || context.data.status == 'active') {
    context.data.isActive = true;
    context.data.cycleStart = Date.now();
    context.data.cycleDuration = config.get('cycleDuration');
    context.data.cycleIsFinished = false;
    context.params.updateStatus = true;
    delete context.data.status;
    delete context.data.restartCycle;
  } else if (context.data.status) {
    context.params.updateStatus = true;
  }
  return context;
};
