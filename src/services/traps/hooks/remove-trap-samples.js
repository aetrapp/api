module.exports = () => async context => {
  if(typeof context.id == "string") {
    await context.app.service("samples").remove(null, { query: { trapId: context.id } });
  }
  return context;
};
