/*
 * Load trap to context if not available
 */

module.exports = () => {
  return async function (context) {
    let { trap } = context;
    if (!trap) {
      const traps = context.app.service("traps");
      trap = await traps.get(context.id, {
        skipResolver: true
      });
      context.trap = trap;
      return context;
    }
  };
};
