const _ = require("lodash");

const { discard, when } = require("feathers-hooks-common");

// auth
const { authenticate } = require("@feathersjs/authentication").hooks;
const { restrictToRoles } = require("feathers-authentication-hooks");
const simplifyGeojson = require("simplify-geojson");

/*
 * Authentication
 */

const restrict = [
  authenticate("jwt"),
  restrictToRoles({
    roles: ["admin"],
    idField: "id"
  })
];

const parseGeodata = () => hook => {
  if (hook.data.geojson) {
    hook.data.simplifiedGeojson = simplifyGeojson(hook.data.geojson, 0.00001);
    hook.data.topojson = {};
  }
  return hook;
};

/*
 * The hooks
 */

module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      ...restrict,
      parseGeodata(),
      context => {
        const { geojson } = context.data;

        if (geojson && geojson.features) {
          let properties = [];

          geojson.features.forEach(feature => {
            properties =
              feature.properties &&
              _.union(properties, _.keys(feature.properties));
          });

          context.data.properties = properties;
        }
      }
    ],
    patch: [...restrict, parseGeodata()],
    remove: [...restrict]
  },

  after: {
    all: [when(hook => hook.params.provider, discard("geojson", "topojson"))],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
