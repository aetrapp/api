const { iff, disallow } = require('feathers-hooks-common');
const updateTrapRelationToCityAverage = require("../../hooks/update-trap-relation-to-city-average");

module.exports = {
  before: {
    find: [],
    get: [],
    create: disallow(),
    update: disallow(),
    patch: [disallow("external")],
    remove: disallow()
  },
  after: {
    patch: [
      iff(
        context => (typeof context.data.eggCountAverage !== "undefined"),
        updateTrapRelationToCityAverage("fromCity")
      ),
      async context => {
        if(!context.id) return context;
        const traps = await context.app.service("traps").find({
          query: {
            cityId: context.id
          },
          skipResolve: true,
          paginate: false
        });
        for(const trap of traps) {
          await context.app.service("traps").patch(trap.id, {}, { updateStatus: true });
        }
        return context;
      }
    ]
  }
};
