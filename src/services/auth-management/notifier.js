const path = require("path");
const pug = require("pug");
const logger = require("winston");

const emailAccountTemplatesPath = path.join(
  __dirname,
  "..",
  "..",
  "email-templates",
  "account"
);

module.exports = function(app) {
  const config = require("config");
  const webappUrl = config.get("webappUrl");
  const returnEmail = config.get("supportEmail");
  const senderEmail = config.get("mailgun").senderEmail;

  if (!senderEmail)
    throw Error("Sender address for e-mail notifiations is not defined.");

  function getLink(type, hash) {
    return `${webappUrl}/login/${type}/${hash}`;
  }

  function sendEmail(email) {
    return app
      .service("emails")
      .create(email)
      .then(function(result) {
        logger.info("Sent email", result);
      })
      .catch(err => {
        logger.error("Error sending email", err);
      });
  }

  return {
    notifier: function(type, user) {
      logger.info(`-- Preparing email for ${type}`);
      var hashLink;
      var email;
      var templatePath;
      var compiledHTML;
      switch (type) {
      case "resendVerifySignup": // send another email with link for verifying user's email addr
        hashLink = getLink("verify", user.verifyToken);

        templatePath = path.join(
          emailAccountTemplatesPath,
          "verify-email.pug"
        );

        compiledHTML = pug.compileFile(templatePath)({
          logo: "",
          name: user.firstName || user.email,
          hashLink,
          returnEmail
        });

        email = {
          from: senderEmail,
          to: user.email,
          subject: "Confirme seu e-mail",
          html: compiledHTML
        };

        return sendEmail(email);
      case "verifySignup": // inform that user's email is now confirmed
        hashLink = getLink("verify", user.verifyToken);

        templatePath = path.join(
          emailAccountTemplatesPath,
          "email-verified.pug"
        );

        compiledHTML = pug.compileFile(templatePath)({
          logo: "",
          name: user.firstName || user.email,
          hashLink,
          returnEmail
        });

        email = {
          from: senderEmail,
          to: user.email,
          subject: "Seu email foi confirmado, obrigado",
          html: compiledHTML
        };

        return sendEmail(email);

      case "sendResetPwd": // inform that user's email is now confirmed
        hashLink = getLink("reset", user.resetToken);

        templatePath = path.join(
          emailAccountTemplatesPath,
          "reset-password.pug"
        );

        compiledHTML = pug.compileFile(templatePath)({
          logo: "",
          name: user.firstName || user.email,
          hashLink,
          returnEmail
        });

        email = {
          from: senderEmail,
          to: user.email,
          subject: "Recupere sua senha",
          html: compiledHTML
        };

        return sendEmail(email);
      case "resetPwd": // inform that user's email is now confirmed
        hashLink = getLink("reset", user.resetToken);

        templatePath = path.join(
          emailAccountTemplatesPath,
          "password-was-reset.pug"
        );

        compiledHTML = pug.compileFile(templatePath)({
          logo: "",
          name: user.firstName || user.email,
          hashLink,
          returnEmail
        });

        email = {
          from: senderEmail,
          to: user.email,
          subject: "Sua senha foi redefinida",
          html: compiledHTML
        };

        return sendEmail(email);

      case "passwordChange":
        templatePath = path.join(
          emailAccountTemplatesPath,
          "password-change.pug"
        );

        compiledHTML = pug.compileFile(templatePath)({
          logo: "",
          name: user.firstName || user.email,
          returnEmail
        });

        email = {
          from: senderEmail,
          to: user.email,
          subject: "Sua senha foi alterada",
          html: compiledHTML
        };

        return sendEmail(email);

      case "identityChange":
        hashLink = getLink("verifyChanges", user.verifyToken);

        templatePath = path.join(
          emailAccountTemplatesPath,
          "identity-change.pug"
        );

        compiledHTML = pug.compileFile(templatePath)({
          logo: "",
          name: user.firstName || user.email,
          hashLink,
          returnEmail,
          changes: user.verifyChanges
        });

        email = {
          from: senderEmail,
          to: user.email,
          subject: "Your account was changed. Please verify the changes",
          html: compiledHTML
        };

        return sendEmail(email);
      default:
        break;
      }
    }
  };
};
