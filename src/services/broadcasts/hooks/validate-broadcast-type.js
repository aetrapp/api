const { getItems } = require("feathers-hooks-common");
const errors = require('feathers-errors');

module.exports = () => {
  return async function (context) {
    const broadcast = getItems(context);

    const validTypes = ['global', 'city', 'direct'];
    if (validTypes.indexOf(broadcast.type) == -1) {
      throw new errors.BadRequest('Invalid broadcast type');
    }

    return context;
  };
};
