const _ = require('lodash');
const logger = require('winston');

// Feathers
const { getItems } = require("feathers-hooks-common");

module.exports = () => {
  return async function (context) {
    const broadcast = getItems(context);

    // Create notifications to users
    const userIds = _.map(_.uniq(broadcast.recipients.users), c => c.toString());
    for (const userId of userIds) {
      await context.app.service("notifications").create({
        type: broadcast.type,
        title: broadcast.title,
        message: broadcast.message,
        deliveryTime: broadcast.deliveryTime,
        broadcastId: broadcast.id,
        recipientId: userId
      }).catch(err => {
        logger.err('Error creating user notification: ', err.message);
      });
    }
  };
};
