const _ = require('lodash');

// Feathers
const { getItems } = require("feathers-hooks-common");
const errors = require('feathers-errors');

// Sequelize
const Sequelize = require("sequelize");
const { Op } = Sequelize;

module.exports = () => {
  return async function (context) {
    const item = getItems(context);

    if (item.type != 'direct') return context;

    // Array of userIds must be passed
    if (!item.recipients || !item.recipients.users || !Array.isArray(item.recipients.users) || item.recipients.users.length == 0) {
      throw new errors.BadRequest('Missing or mal-formed "recipient.users" property, it must be an Array of valid city ids.');
    }

    // Verify if users passed exist
    const userIds = _.map(_.uniq(item.recipients.users), c => c.toString());
    const usersService = context.app.service("users");
    const queryResult = await usersService.find({
      query: {
        id: {
          [Op.in]: userIds
        },
        $limit: 0,
      },
      skipResolver: true
    }).catch(() => {
      throw new errors.GeneralError('Unexpected error');
    });

    // Count of users must be equal of ids passed
    if (queryResult.total != userIds.length) {
      throw new errors.BadRequest('User ids of array in "recipient.users" were not found.');
    }

    return context;
  };
};
