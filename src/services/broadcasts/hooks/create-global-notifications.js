const logger = require('winston');

// Feathers
const { getItems } = require("feathers-hooks-common");

module.exports = () => {
  return async function (context) {
    const notificationsService = context.app.service("notifications");
    const usersService = context.app.service("users");

    const broadcast = getItems(context);

    // Get users
    const allUsers = await usersService.find({
      query: {
        $select: ['id']
      },
      paginate: false,
      skipResolver: true,
    });

    // Create notifications to all
    for (const user of allUsers) {
      await notificationsService.create({
        type: broadcast.type,
        title: broadcast.title,
        message: broadcast.message,
        deliveryTime: broadcast.deliveryTime,
        broadcastId: broadcast.id,
        recipientId: user.id
      }).catch(err => {
        logger.err('Error creating global notification: ', err.message);
      });
    }

    return context;
  };
};
