const _ = require('lodash');
const logger = require('winston');

// Feathers
const { getItems } = require("feathers-hooks-common");

// Sequelize
const Sequelize = require("sequelize");
const { Op } = Sequelize;

module.exports = () => {
  return async function (context) {
    const notificationsService = context.app.service("notifications");
    const trapsService = context.app.service("traps");

    const broadcast = getItems(context);

    // Get active traps in city
    const cityIds = _.map(_.uniq(broadcast.recipients.cities), c => c.toString());
    const traps = await trapsService.find({
      query: {
        $select: ['id', 'ownerId'],
        isActive: true,
        cityId: {
          [Op.in]: cityIds
        }
      },
      paginate: false,
      skipResolver: true,
    });

    // Create notifications to users with active traps
    const recipientUserIds = _.uniq(_.map(traps, trap => trap.ownerId));
    for (const userId of recipientUserIds) {
      await notificationsService.create({
        type: broadcast.type,
        title: broadcast.title,
        message: broadcast.message,
        deliveryTime: broadcast.deliveryTime,
        broadcastId: broadcast.id,
        recipientId: userId
      }).catch(err => {
        logger.err('Error creating city notification: ', err.message);
      });
    }
  };
};
