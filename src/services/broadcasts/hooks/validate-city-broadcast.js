const _ = require('lodash');

// Feathers
const { getItems } = require("feathers-hooks-common");
const errors = require('feathers-errors');

// Sequelize
const Sequelize = require("sequelize");
const { Op } = Sequelize;

module.exports = () => {
  return async function (context) {
    const item = getItems(context);
    const { user } = context.params;

    if (item.type != 'city') return context;

    // Array of cityIds must be passed
    if (!item.recipients || !item.recipients.cities || !Array.isArray(item.recipients.cities) || item.recipients.cities.length == 0) {
      throw new errors.BadRequest('Missing or mal-formed "recipient.cities" property, it must be an Array of valid city ids.');
    }

    // Verify if cities exist
    const cityIds = _.map(_.uniq(item.recipients.cities), c => c.toString());
    const citiesService = context.app.service("cities");
    const queryResult = await citiesService.find({
      query: {
        id: {
          [Op.in]: cityIds
        },
        $limit: 0,
      },
      skipResolver: true
    }).catch(() => {
      throw new errors.GeneralError('Unexpected error');
    });
    if (queryResult.total != cityIds.length) {
      throw new errors.BadRequest('City ids of array in "recipient.cities" were not found.');
    }

    // If user is a moderator, verify if allowed to broadcast in the city
    if (user.roles.indexOf('moderator') > -1) {
      const { users } = context.app.get("sequelizeClient").models;
      const sequelizeUser = await users.findById(user.id);
      const cities = await sequelizeUser.getCities();
      const allowedCities = _.map(cities, c => c.id);
      for (const cityId of item.recipients.cities) {
        if (allowedCities.indexOf(cityId.toString()) == -1) {
          throw new errors.Forbidden(`User is not allowed to send broadcast to city (${cityId}).`);
        }
      }
    }

    // Check if there are active traps in city
    const traps = await context.app.service("traps").find({
      query: {
        $select: ['id', 'ownerId'],
        isActive: true,
        cityId: {
          [Op.in]: cityIds
        }
      },
      paginate: false,
      skipResolver: true,
    });

    // There are no active traps in city
    if (!traps || traps.length == 0) {
      throw new errors.GeneralError('City does not have active traps.');
    }

    return context;
  };
};
