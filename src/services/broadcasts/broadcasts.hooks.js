// Feathers Hooks Common
const {
  iff,
  iffElse,
} = require("feathers-hooks-common");

// Auth hooks
const { authenticate } = require('@feathersjs/authentication').hooks;
const { restrictToRoles } = require("feathers-authentication-hooks");

// Custom hooks
const createDirectNotification = require('./hooks/create-direct-notifications');
const createCityNotification = require('./hooks/create-city-notifications');
const createGlobalNotification = require('./hooks/create-global-notifications');
const validateBroadcastType = require('./hooks/validate-broadcast-type');
const validateCityBroadcast = require('./hooks/validate-city-broadcast');
const validateDirectBroadcast = require('./hooks/validate-city-broadcast');

// Restriction hooks
const restrictToOwnerModeratorOrAdmin = [
  restrictToRoles({
    roles: ['admin', 'moderator'],
    idField: "id",
    ownerField: "id",
    owner: true
  })
];

const restrictToModeratorOrAdmin = [
  restrictToRoles({
    roles: ['admin', 'moderator'],
    idField: "id"
  })
];

const restrictToAdmin = [
  restrictToRoles({
    roles: ['admin'],
    idField: "id"
  })
];


module.exports = {
  before: {
    all: [authenticate("jwt")],
    find: [...restrictToModeratorOrAdmin],
    get: [...restrictToOwnerModeratorOrAdmin],
    create: [
      validateBroadcastType(),
      iffElse(
        context => context.data.type && context.data.type == 'city',
        restrictToModeratorOrAdmin,
        restrictToAdmin
      ),
      validateCityBroadcast(),
      validateDirectBroadcast()
    ],
    remove: [...restrictToOwnerModeratorOrAdmin]
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [
      iff(context => context.data.type && context.data.type == 'direct', createDirectNotification()),
      iff(context => context.data.type && context.data.type == 'global', createGlobalNotification()),
      iff(context => context.data.type && context.data.type == 'city', createCityNotification())
    ],
    remove: []
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    remove: []
  }
};
