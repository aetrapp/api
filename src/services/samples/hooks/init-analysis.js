// Logger
const logger = require('winston');

// HTTP Client
const axios = require('axios');

// Config
const config = require('config');
const isTesting = config.get('isTesting');
const apiUrl = config.get('apiUrl');
const ipsUrl = config.get("ipsUrl") + "/agenda/api/jobs/create";

// Feathers Errors
const errors = require("@feathersjs/errors");

// Helpers
const generateId = require("../../../helpers/generate-id");

module.exports = function () {
  return async function (context) {

    const jobId = generateId();

    // Fake Job id when testing
    if (isTesting) {
      if (context.data.fakeIpsInitAnalysisSuccess) {
        context.data = Object.assign(context.data, {
          status: "analysing",
          error: null,
          jobId: jobId
        });
        return context;
      } else if (context.data.skipAnalysis) {
        return context;
      }
    }

    let blobId;
    switch (context.method) {
    case "create":
      blobId = context.data.blobId;
      break;
    case "patch":
      blobId = context.sample.blobId;
      break;
    default:
      return context;
    }

    let res = await axios
      .post(ipsUrl, {
        jobName: "process image",
        jobSchedule: "now",
        jobData: {
          image: {
            url: `${apiUrl}/files/${blobId}`
          },
          webhookUrl: `${apiUrl}/samples/analysis/${jobId}`
        }
      }).catch((err) => {
        logger.error('Error registering sample in IPS: ' + err.message);
        throw new errors.GeneralError('O serviço de imagens está indisponível. Por favor, tente novamente mais tarde.');
      });

    if (res) {
      context.data = Object.assign(context.data, {
        status: "analysing",
        error: null,
        jobId
      });
    }

    return context;
  };
};
