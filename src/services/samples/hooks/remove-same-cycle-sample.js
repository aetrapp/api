const Sequelize = require("sequelize");
const { Op } = Sequelize;

module.exports = () => async context => {
  const trap = context.trap;
  const sample = context.sample || context.result;
  if (sample.collectedAt > trap.cycleStart) {
    await context.service.remove(null, {
      query: {
        id: { [Op.ne]: sample.id },
        trapId: sample.trapId,
        collectedAt: {
          [Op.gt]: trap.cycleStart
        }
      },
      paginate: false
    });
  }
  return context;
};
