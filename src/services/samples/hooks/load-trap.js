const _ = require("lodash");
const errors = require("@feathersjs/errors");
const { getItems } = require('feathers-hooks-common');

module.exports = () => {
  return async function (context) {
    let { trap } = context;
    const sample = getItems(context) || context.sample;

    if(!sample) {
      return context;
    }

    let trapId;

    if(Array.isArray(sample)) {
      if(!sample.length) {
        return context;
      }
      const trapIds = _.uniq(_.map(sample, s => s.trapId));
      if(trapIds.length > 1) {
        throw new errors.BadRequest("You cant batch modify samples from different traps");
      }
      trapId = trapIds[0];
    } else {
      trapId = sample.trapId;
    }

    if (!trap) {
      const traps = context.app.service("traps");
      trap = await traps.get(trapId, {
        skipResolver: true
      }).catch(() => { return new errors.BadRequest("Could not find sample."); });
      context.trap = trap;
      return context;
    }
  };
};
