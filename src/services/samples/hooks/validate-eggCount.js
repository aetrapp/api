module.exports = () => async context => {
  if (context.data.status == "invalid") {
    context.data.eggCount = null;
  }
  return context;
};
