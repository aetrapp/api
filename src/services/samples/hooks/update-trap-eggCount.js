const moment = require("moment");

// Sequelize
const Sequelize = require("sequelize");
const { Op } = Sequelize;

const endOfLastWeek = moment()
  .subtract(14, "days")
  .endOf("isoWeek")
  .toDate();

module.exports = () => async context => {

  // if trap is not defined, bypass hook
  if (!context.trap) {
    return context;
  }

  // when a sample is changed, its trap should have a status update
  if (context.method == "remove" || (typeof context.data.eggCount != 'undefined') || context.data.status == "invalid") {
    const trapLastSample = await context.service.find({
      query: {
        trapId: context.trap.id,
        $limit: 1,
        status: "valid",
        collectedAt: { [Op.gt]: endOfLastWeek },
        $sort: { collectedAt: -1 }
      },
      paginate: false
    });

    if (trapLastSample && trapLastSample.length) {
      // trap has a valid recent sample. If sample is in same cycle, update cycleIsFinished to true
      await context.app
        .service("traps")
        .patch(context.trap.id, {
          eggCount: trapLastSample[0].eggCount,
          eggCountDate: trapLastSample[0].collectedAt,
          cycleIsFinished: trapLastSample[0].collectedAt > context.trap.cycleStart
        });
    } else {

      // trap does not have a recent sample
      await context.app
        .service("traps")
        .patch(context.trap.id, { eggCount: null, eggCountDate: null, cycleIsFinished: false });
    }
  }
  return context;
};
