module.exports = () => async context => {

  // if trap is not defined, bypass hook
  if (!context.trap) {
    return context;
  }

  // get sample count
  const queryResult = await context.app.service("samples").find({
    query: {
      trapId: context.trap.id,
      $limit: 0
    }
  });

  // update trap
  await context.app
    .service("traps")
    .patch(context.trap.id, {
      sampleCount: queryResult ? queryResult.total : 0
    });

  return context;
};
