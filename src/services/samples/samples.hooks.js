const _ = require("lodash");
const logger = require("winston");

// Feathers & Sequelize
const { authenticate } = require("@feathersjs/authentication").hooks;
const {
  restrictToRoles,
  associateCurrentUser
} = require("feathers-authentication-hooks");
const { iff, getItems } = require("feathers-hooks-common");

// Helper hooks
const { doResolver } = require('../../hooks');
const loadTrap = require('./hooks/load-trap');
const loadSample = require('./hooks/load-sample');
const initAnalysis = require('./hooks/init-analysis');
const updateTrapRelationToCityAverage = require("../../hooks/update-trap-relation-to-city-average");
const updateCityStatistics = require("../../hooks/update-city-statistics");
const updateTrapEggCount = require("./hooks/update-trap-eggCount");
const updateTrapSampleCount = require("./hooks/update-trap-sample-count");
const validateEggCount = require("./hooks/validate-eggCount");
const removeSameCycleSamples = require("./hooks/remove-same-cycle-sample");
const parseSortByCity = require('../../hooks/parse-sort-by-city');

/*
 * Hooks
 */

const restrict = [
  authenticate("jwt"),
  restrictToRoles({
    roles: ['admin'],
    idField: "id",
    ownerField: "ownerId",
    owner: true
  })
];

const handleAverageUpdates = [
  loadTrap(),
  updateTrapEggCount(),
  updateCityStatistics(),
  iff(
    context => (typeof context.result.eggCount !== "undefined"),
    updateTrapRelationToCityAverage("fromSample")
  ),
  async context => {
    if (context.trap) {
      await context.app.service("traps").patch(context.trap.id, {}, { updateStatus: true });
    }
    return context;
  }
];

const storeBlob = function () {
  return function (hook) {
    const blobService = hook.app.service("uploads");
    return blobService.create({ uri: hook.data.base64 }).then(res => {
      hook.data.blobId = res.id;
      return hook;
    });
  };
};

const addNotification = function () {
  return function (hook) {
    const sample = _.castArray(getItems(hook))[0];

    // "Sample is finished" notification
    hook.app
      .service("notifications")
      .create({
        recipientId: sample.ownerId,
        payload: {
          type: 'sample-analysis-finished',
          deeplink: 'sample/' + sample.id,
          sampleId: sample.id
        },
        title: "Análise de amostra concluída",
        message: "Estão disponíveis os resultados da amostra " + sample.id + "."
      })
      .catch(err => {
        logger.error('Error creating notification');
        logger.error(err);
      });
  };
};


const sampleResolvers = {
  joins: {
    owner: () => async (sample, context) => {
      const users = context.app.services['users'];
      try {
        sample.owner = (await users.get(sample.ownerId, {
          skipResolver: true,
          query: {
            $select: ['id', 'firstName', 'lastName']
          }
        }));
      } catch (error) {
        logger.error(`User ${sample.ownerId} not found for sample ${sample.id}.`);
      }
    },
    city: () => async (sample, context) => {
      const cities = context.app.services['cities'];
      sample.city = await cities.get(sample.cityId, {
        skipResolver: true
      });
    }
  }
};

module.exports = {
  before: {
    all: [],
    find: [
      parseSortByCity()
    ],
    get: [],
    create: [
      authenticate("jwt"),
      associateCurrentUser({ idField: "id", as: "ownerId" }),
      loadTrap(),
      // Add cityId to sample
      context => {
        context.data.cityId = context.trap.cityId;
        return context;
      },
      storeBlob(),
      initAnalysis(),
    ],
    patch: [
      ...restrict,
      validateEggCount(),
      iff(
        hook => { return hook.data && hook.data.status == "analysing" && !hook.data.jobId; },
        [loadSample(), initAnalysis()]
      )
    ],
    remove: [...restrict, loadSample(), loadTrap()]
  },

  after: {
    all: [],
    find: [doResolver(sampleResolvers)],
    create: [
      removeSameCycleSamples(),
      updateTrapSampleCount(),
      ...handleAverageUpdates
    ],
    patch: [
      ...handleAverageUpdates,
      iff(
        hook => { return hook.data && hook.data.status != "analysing"; },
        addNotification()
      )
    ],
    remove: [
      updateTrapSampleCount(),
      ...handleAverageUpdates
    ]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    patch: [],
    remove: []
  }
};
