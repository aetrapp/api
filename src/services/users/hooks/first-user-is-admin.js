module.exports = () => {
  return async function (context) {
    // get users service
    const users = await context.app.service("users");

    // query for user count
    const usersQuery = await users.find({
      skipResolver: true,
      $limit: 0
    });

    // first user should have admin role
    if (usersQuery.total == 0) {
      context.data.roles = ['admin'];
    }

    return context;
  };
};
