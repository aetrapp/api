// Feathers Hooks Common
const {
  discard,
  disallow,
  fastJoin,
  iff,
  isProvider,
  keep,
  when
} = require("feathers-hooks-common");

// Auth hooks
const { authenticate } = require('@feathersjs/authentication').hooks;
const verifyHooks = require('feathers-authentication-management').hooks;
const { restrictToRoles } = require("feathers-authentication-hooks");
const { hashPassword } = require('@feathersjs/authentication-local').hooks;

// Helper hooks
const {  sendVerificationEmail } = require('../../hooks');
const firstUserIsAdmin = require('./hooks/first-user-is-admin');

// Restriction hooks
const restrictToOwnerOrAdmin = [
  authenticate("jwt"),
  restrictToRoles({
    roles: ['admin'],
    idField: "id",
    ownerField: "id",
    owner: true
  })
];

const restrictToAdmin = [
  restrictToRoles({
    roles: ['admin'],
    idField: "id"
  })
];

const trapResolver = {
  joins: {
    trapCount: () => async (user, context) => {
      const traps = context.app.services['traps'];
      const queryResult = await traps.find({
        query: {
          $limit: 0,
          ownerId: user.id
        },
      });
      user.trapCount = queryResult.total;
    }
  }
};

const changeableUserProperties = [
  'firstName',
  'lastName',
  'landlineNumber',
  'cellphoneNumber',
  'dateOfBirth',
  'gender',
  'password'
];

module.exports = {
  before: {
    all: [],
    find: [],
    get: [...restrictToOwnerOrAdmin],
    create: [
      keep(...changeableUserProperties.concat('email')),
      hashPassword(),
      verifyHooks.addVerification(),
      firstUserIsAdmin()
    ],
    update: [disallow('external')],
    patch: [
      ...restrictToOwnerOrAdmin,
      iff(context => context.data.roles, restrictToAdmin),
      keep(...changeableUserProperties.concat('roles'))
    ],
    remove: [...restrictToOwnerOrAdmin]
  },

  after: {
    all: [
      when(
        hook => hook.params.provider,
        discard('password', '_computed', 'verifyExpires', 'resetExpires', 'verifyChanges')
      ),
    ],
    find: [
      // do not expose user data if user is not logged
      iff(
        isProvider('external'),
        iff(hook => !hook.params.user, [keep("email")])
      )
    ],
    get: [iff(isProvider('external'), fastJoin(trapResolver))],
    create: [
      sendVerificationEmail(),
      verifyHooks.removeVerification(),
    ],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
