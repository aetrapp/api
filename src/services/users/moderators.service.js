const logger = require('winston');

// Feathers
const errors = require('@feathersjs/errors');
const { authenticate } = require('@feathersjs/authentication').hooks;
const { restrictToRoles } = require("feathers-authentication-hooks");

module.exports = app => {

  // Authentication
  const restrictToAdmin = [
    authenticate("jwt"),
    restrictToRoles({
      roles: ['admin'],
      idField: "id"
    })
  ];

  const restrictToAdminOrModerator = [
    authenticate("jwt"),
    restrictToRoles({
      roles: ['admin', 'moderator'],
      idField: "id"
    })
  ];


  const hooks = {
    before: {
      find: [...restrictToAdminOrModerator],
      create: [...restrictToAdmin],
      remove: [...restrictToAdmin]
    }
  };

  // Find and create new cities relations
  app.use("/users/:userId/allowedCities", {
    async find(params) {
      const { users } = app.get("sequelizeClient").models;
      const { userId } = params.route;

      // Validate user
      if (!userId)
        throw new errors.NotFound("Invalid route.");

      const user = await users.findById(userId).catch(() => {
        throw new errors.GeneralError('Could not associate user to city.');
      });

      if (!user)
        throw new errors.NotFound("User not found.");

      const cities = await user.getCities().catch(() => {
        throw new errors.GeneralError('Error loading cities.');
      });

      return cities;
    },
    async create(data, params) {
      const { cities, users } = app.get("sequelizeClient").models;
      const { cityId } = data;
      const { userId } = params.route;

      // Validate user
      if (!userId)
        throw new errors.NotFound("Invalid route.");

      const user = await users.findById(userId).catch(() => {
        throw new errors.GeneralError('Could not associate user to city.');
      });

      if (!user)
        throw new errors.NotFound("User not found.");

      // Validate city
      if (!cityId)
        throw new errors.NotFound("Query parameter cityId is required.");

      const city = await cities.findById(cityId.toString()).catch(() => {
        throw new errors.GeneralError('Could not associate user to city.');
      });

      if (!city)
        throw new errors.NotFound("City not found.");

      await city.addModerator(userId).catch(err => {
        logger.error('Error associating user to city: ', err.message);
        throw new errors.GeneralError('Could not associate user to city.');
      });

      return {};
    },
    async remove(cityId, params) {
      const { cities, users } = app.get("sequelizeClient").models;
      const { userId } = params.route;

      // Validate user
      if (!userId)
        throw new errors.NotFound("Invalid route.");

      const user = await users.findById(userId).catch(() => {
        throw new errors.GeneralError('Could not associate user to city.');
      });

      if (!user)
        throw new errors.NotFound("User not found.");

      // Validate city
      if (!cityId)
        throw new errors.NotFound("Invalid route.");

      const city = await cities.findById(cityId).catch(() => {
        throw new errors.GeneralError('Could not remove user from city.');
      });

      if (!city)
        throw new errors.NotFound("City not found.");

      await city.removeModerator(userId).catch(() => {
        throw new errors.GeneralError('Could not remove user from city.');
      });

      return {};
    }
  });

  // Add hooks
  app.service("/users/:userId/allowedCities").hooks(hooks);

};
