/* eslint-disable no-console */

const logger = require('winston');

// App
const app = require('./app');
const port = app.get('port');
const server = app.listen(port);

server.on('listening', () => {
  logger.info('Aetrapp Data Service started on http://%s:%d', app.get('host'), port);

  // Schedule job
  const updateDelayedTraps = require('./jobs/update-delayed-traps');
  setInterval(() => updateDelayedTraps(app), 5000);

  // Verify if IPS version was updated and run new analisys
  const verifyIpsVersionChange = require('./jobs/verify-ips-version-change');
  verifyIpsVersionChange(app);
  setInterval(() => verifyIpsVersionChange(app), 5 * 60 * 1000);

  // Check if there are abandoned traps
  const deactivateAbandonedTraps = require('./jobs/deactivate-abandoned-traps');
  deactivateAbandonedTraps(app);
  setInterval(() => deactivateAbandonedTraps(app), 5 * 60 * 1000);
});

// Catch unhandled promises
process.on('unhandledRejection', (reason, p) =>
  logger.error('Unhandled Rejection at: Promise ', p, reason)
);
