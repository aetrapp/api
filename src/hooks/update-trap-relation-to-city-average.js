const errors = require("@feathersjs/errors");

module.exports = origin => async context => {
  const { app } = context;
  const trapsService = app.service("traps");
  const citiesService = app.service("cities");

  let traps = [];
  let city;

  if (origin == "fromSample") {
    const trap = await trapsService.get(context.result.trapId, {
      skipResolver: true
    });
    city = await citiesService.get(trap.cityId, { skipResolver: true });
    traps = [trap];
  } else if (origin == "fromCity") {
    city = context.result;
    traps = await trapsService.find({
      query: {
        cityId: city.id
      },
      paginate: false,
      skipResolver: true
    });
  } else {
    throw new errors.GeneralError("Must provide trapId or city object.");
  }

  if (!city.eggCountAverage) return context;

  for (const trap of traps) {
    let relationToCityAverage = null;
    if (trap.eggCount) {
      relationToCityAverage =
        city.eggCountAverage >= trap.eggCount ? "below-average" : "above-average";
    }
    await trapsService.patch(
      trap.id,
      {
        relationToCityAverage
      },
      { skipResolver: true }
    );
  }

  return context;
};
