const _ = require('lodash');
const { getItems } = require("feathers-hooks-common");
const moment = require('moment');
const { Op } = require('sequelize');

module.exports = () => {
  return async context => {
    let cityId;
    const item = getItems(context);
    if(context.path == "samples") {
      if(context.trap) {
        cityId = context.trap.cityId;
      } else {
        return context;
      }
    } else if(context.path == "traps") {
      cityId = item.cityId;
    }
    const traps = context.app.service("traps");
    const samples = context.app.service("samples");
    const cities = context.app.service("cities");

    // get traps belonging to city
    const trapIds = await traps.find({
      skipResolver: true,
      query: {
        $select: ['id'],
        cityId
      },
      paginate: false
    });

    // get last week finish date
    const lastWeekFinishedAt = moment().subtract(7, 'days').endOf('isoWeek');

    // get valid samples until last week
    let validSamples = await samples.find({
      skipResolver: true,
      query: {
        status: 'valid',
        collectedAt: {
          [Op.lte]: lastWeekFinishedAt.toDate()
        },
        trapId: trapIds.map(trap => trap.id),
        $select: ['id', 'eggCount', 'collectedAt'],
        $sort: { collectedAt: -1 }
      },
      paginate: false
    });

    // do not calculate if there are no samples for the city
    if (!validSamples || validSamples.length == 0) return context;

    // aggregate in weeks
    validSamples = validSamples.map(sample => {
      const collectedAt = moment(sample.collectedAt);
      sample.week = collectedAt.format('YYYY-w');
      return sample;
    });
    let weeks = _.groupBy(validSamples, 'week');

    // transform to hash
    for (const weekId in weeks) {
      if (weeks.hasOwnProperty(weekId)) {
        weeks[weekId] = Math.round(_.meanBy(weeks[weekId], 'eggCount') * 100) / 100;
      }
    }

    // get last valid sample week
    const lastValidCountDate = moment(validSamples[0].collectedAt);

    await cities.patch(cityId, {
      eggCountAverage: weeks[lastValidCountDate.format('YYYY-w')],
      eggCountAverageDate: lastWeekFinishedAt.endOf('isoWeek').toDate(),
      eggCountAverages: weeks
    });

    return context;
  };
};
