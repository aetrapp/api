const logger = require('winston');

module.exports = function (app) {
  if (typeof app.channel !== 'function') {
    // If no real-time functionality has been configured just return
    return;
  }

  app.on('login', (authResult, { connection }) => {
    // connection can be undefined if there is no
    // real-time connection, e.g. when logging in via REST
    if (connection) {
      const user = connection.user;

      // create user specific channel
      app.channel(`user/${user.id}`).join(connection);
      logger.info(`User ${user.firstName} (${user.id}) has logged in.`);
    }
  });

  // Bind service events to user specific channels
  app.service('users').publish((data) => { return app.channel(`user/${data.id}`); });
  app.service('traps').publish((data) => { return app.channel(`user/${data.ownerId}`); });
  app.service('samples').publish((data) => { return app.channel(`user/${data.ownerId}`); });
  app.service('notifications').publish((data) => { return app.channel(`user/${data.recipientId}`); });

};
