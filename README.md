# AeTrapp Data Service 

> AeTrapp is a platform to monitor [Aedes aegypti](https://en.wikipedia.org/wiki/Aedes_aegypti) populations. This service provides an editing API of AeTrapp data.

![Aedes Aegypti](aedes-aegypti.jpg)

Check our [contributing guide](CONTRIBUTING.md) and feel free to open issues. 

## Getting started

Install dependencies:

- [git](https://git-scm.com)
- [nvm](https://github.com/creationix/nvm)
- [yarn](https://yarnpkg.com)
- [Docker](https://www.docker.com/)

[Clone this repository](https://help.github.com/en/articles/cloning-a-repository) and activate the required Node.js version by running:

`nvm install`

The last step can be skipped if the local Node.js version matches the one defined at [.nvmrc](.nvmrc). 

Install module dependencies:

`yarn install`

## Development

Start the development database:

    yarn start-dev-db

When the database is ready for connections, open a new terminal to run migrations and seeders:

    yarn migrate-dev-db
    yarn seed-dev-db

Start the development server:

    yarn dev

If the start is successful, a list of cities should be accessible at http://localhost:3030/cities.

When developing is finished, kill the database container with `Control+C`.

### Testing 

Start the testing database:

    yarn start-test-db

After the database is ready for connections, run tests:

    yarn test

When developing is finished, kill the database container with `Control+C`.

### Update config file (optional)

To enable transactional emails, notifications and image processing service, change file `config/development.json` like in this example:

```json
{
  "ipsUrl": "http://<ips_hostname>:<ips_port>",
  "mailgun": {
    "api_key": "<mailgun_api_key>",
    "domain": "<mailgun_domain>",
    "senderEmail": "<sender_email>"
  },
  "oneSignal": {
    "API_KEY": "<onesignal_api_key>",
    "APP_ID": "<onesignal_app_id>"
  }
}
```

Check `config/default.json` for all available options.

## License

[GPL-3.0](LICENSE)
