"use strict";

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn("traps", "addressNumber");
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "traps",
      "addressNumber",
      Sequelize.DataTypes.STRING
    );
  }
};
