"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "samples",
      "ipsData",
      Sequelize.DataTypes.JSONB
    );
  },
  down: queryInterface => {
    return queryInterface.removeColumn("samples", "ipsData");
  }
};
