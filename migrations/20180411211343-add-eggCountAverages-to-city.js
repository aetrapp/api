"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "cities",
      "eggCountAverages",
      Sequelize.DataTypes.JSONB
    );
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn("cities", "eggCountAverages");
  }
};
