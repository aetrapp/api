"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "users",
      "trapCount",
      Sequelize.DataTypes.INTEGER
    );
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn("users", "trapCount");
  }
};
