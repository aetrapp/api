"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "cities",
      "eggCountAverageDate",
      Sequelize.DataTypes.DATE
    );
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn("cities", "eggCountAverageDate");
  }
};
