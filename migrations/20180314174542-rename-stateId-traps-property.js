"use strict";

module.exports = {
  up: (queryInterface) => {
    return queryInterface.renameColumn("traps", "addressStateId", "stateId");
  },

  down: (queryInterface) => {
    return queryInterface.renameColumn("traps", "stateId", "addressStateId");
  }
};
