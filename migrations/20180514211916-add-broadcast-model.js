"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    const { DataTypes } = Sequelize;
    return queryInterface.createTable("broadcasts", {
      id: {
        type: DataTypes.STRING,
        primaryKey: true
      },
      type: {
        type: DataTypes.STRING
      },
      title: {
        type: DataTypes.STRING,
        allowNull: false
      },
      message: {
        type: DataTypes.TEXT,
        allowNull: false
      },
      payload: {
        type: DataTypes.JSONB
      },
      recipients: {
        type: DataTypes.JSONB
      },
      status: {
        type: DataTypes.JSONB
      },
      deliveryTime: {
        type: DataTypes.DATE
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable("broadcasts");
  }
};
