"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "notifications",
      "broadcastId",
      Sequelize.DataTypes.STRING
    );
  },
  down: queryInterface => {
    return queryInterface.removeColumn("notifications", "broadcastId");
  }
};
