"use strict";

module.exports = {
  up: (queryInterface) => {
    return queryInterface.removeColumn("traps", "endDate");
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn("traps", "endDate", Sequelize.DATE);
  }
};
