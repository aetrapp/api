"use strict";

module.exports = {
  up: (queryInterface) => {
    return queryInterface.renameColumn(
      "traps",
      "addressNeighborhood",
      "neighborhood"
    );
  },

  down: (queryInterface) => {
    return queryInterface.renameColumn(
      "traps",
      "neighborhood",
      "addressNeighborhood"
    );
  }
};
