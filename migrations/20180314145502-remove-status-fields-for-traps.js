"use strict";

module.exports = {
  up: async queryInterface => {
    return [
      queryInterface.removeColumn("traps", "statusCode"),
      queryInterface.removeColumn("traps", "statusMessage")
    ];
  },

  down: async (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn(
        "traps",
        "statusCode",
        Sequelize.DataTypes.INTEGER
      ),
      queryInterface.addColumn(
        "traps",
        "statusMessage",
        Sequelize.DataTypes.STRING
      )
    ];
  }
};
