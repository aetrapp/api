"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    const { DataTypes } = Sequelize;
    return queryInterface.createTable("citiesModerators", {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      userId: {
        type: DataTypes.STRING,
        references: {
          model: "users",
          key: "id"
        },
        allowNull: false
      },
      cityId: {
        type: DataTypes.STRING,
        references: {
          model: "cities",
          key: "id"
        },
        allowNull: false
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false
      }
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable("citiesModerators");
  }
};
