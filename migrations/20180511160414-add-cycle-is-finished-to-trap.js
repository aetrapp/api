"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "traps",
      "cycleIsFinished",
      Sequelize.DataTypes.BOOLEAN
    );
  },
  down: queryInterface => {
    return queryInterface.removeColumn("traps", "cycleIsFinished");
  }
};
