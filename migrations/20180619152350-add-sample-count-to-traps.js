"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "traps",
      "sampleCount",
      Sequelize.DataTypes.INTEGER
    );
  },
  down: queryInterface => {
    return queryInterface.removeColumn("traps", "sampleCount");
  }
};
