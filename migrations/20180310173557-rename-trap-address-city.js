"use strict";

module.exports = {
  up: (queryInterface) => {
    return queryInterface.renameColumn("traps", "addressCityId", "cityId");
  },

  down: (queryInterface) => {
    return queryInterface.renameColumn("traps", "cityId", "addressCityId");
  }
};
