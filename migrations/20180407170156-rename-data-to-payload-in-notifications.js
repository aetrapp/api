"use strict";

module.exports = {
  up: (queryInterface) => {
    return queryInterface.renameColumn("notifications", "data", "payload");
  },

  down: (queryInterface) => {
    return queryInterface.renameColumn("notifications", "payload", "data");
  }
};
