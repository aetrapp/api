"use strict";

module.exports = {
  up: (queryInterface) => {
    return queryInterface.renameColumn(
      "traps",
      "neighborhood",
      "neighbourhood"
    );
  },

  down: (queryInterface) => {
    return queryInterface.renameColumn(
      "traps",
      "neighbourhood",
      "neighborhood"
    );
  }
};
