"use strict";

module.exports = {
  up: (queryInterface) => {
    return queryInterface.renameColumn("traps", "addressPostcode", "postcode");
  },

  down: (queryInterface) => {
    return queryInterface.renameColumn("traps", "postcode", "addressPostcode");
  }
};
