"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return [
      queryInterface.addColumn("layers", "topojson", Sequelize.DataTypes.JSON),
      queryInterface.addColumn(
        "layers",
        "simplifiedGeojson",
        Sequelize.DataTypes.JSON
      )
    ];
  },
  down: async queryInterface => {
    return [
      queryInterface.removeColumn("layers", "topojson"),
      queryInterface.removeColumn("layers", "simplifiedGeojson")
    ];
  }
};
