"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "traps",
      "eggCountDate",
      Sequelize.DataTypes.DATE
    );
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn("traps", "eggCountDate");
  }
};
