"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "traps",
      "relationToCityAverage",
      Sequelize.DataTypes.STRING
    );
  },
  down: queryInterface => {
    return queryInterface.removeColumn("traps", "relationToCityAverage");
  }
};
