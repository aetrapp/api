"use strict";

module.exports = {
  up: queryInterface => {
    return queryInterface.removeColumn("traps", "startDate");
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn("traps", "startDate", Sequelize.DATE);
  }
};
