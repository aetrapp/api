"use strict";

module.exports = {
  up: function(queryInterface) {
    return queryInterface.sequelize.query(
      'ALTER TABLE "traps" ALTER COLUMN "endDate" DROP NOT NULL;'
    );
  },

  down: async function() {}
};
