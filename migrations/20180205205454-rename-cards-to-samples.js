"use strict";

module.exports = {
  up: (queryInterface) => {
    return queryInterface.renameTable("cards", "samples");
  },
  down: queryInterface => {
    return queryInterface.renameTable("samples", "cards");
  }
};
