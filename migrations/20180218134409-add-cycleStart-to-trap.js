"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "traps",
      "cycleStart",
      Sequelize.DataTypes.DATE
    );
  },

  down: (queryInterface) => {
    return queryInterface.removeColumn("traps", "cycleStart");
  }
};
