"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn("cards", "collectedAt", Sequelize.DATE);
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn("cards", "collectedAt");
  }
};
