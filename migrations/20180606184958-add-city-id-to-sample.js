"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "samples",
      "cityId",
      Sequelize.DataTypes.STRING
    );
  },
  down: queryInterface => {
    return queryInterface.removeColumn("samples", "cityId");
  }
};
