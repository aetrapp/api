"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      "traps",
      "status",
      Sequelize.DataTypes.STRING
    );
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn("traps", "status");
  }
};
