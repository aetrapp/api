"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn("traps", "isActive", Sequelize.BOOLEAN);
  },
  down: (queryInterface) => {
    return queryInterface.removeColumn("traps", "isActive");
  }
};
