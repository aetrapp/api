const config = require("config");
const env = process.env.NODE_ENV || "development";

/*
 * This file is used by Sequelize to load configuration when running migrations.
 * See: https://sequelize.readthedocs.io/en/v3/docs/migrations/
 */

module.exports = {
  [env]: {
    ...config.get("sequelize"),
    seederStorage: "sequelize"
  }
};
