
const axios = require('axios');
const io = require('socket.io-client');
const feathers = require('@feathersjs/feathers');
const auth = require('@feathersjs/authentication-client');
const socketio = require('@feathersjs/socketio-client');

module.exports = async (app, user) => {

  const feathersClient = feathers();

  const port = app.get('port');

  const socket = io(`http://localhost:${port}`, {
    transports: ['websocket'],
    forceNew: true
  });

  // instanciate feathers client
  feathersClient
    .configure(socketio(socket))
    .configure(auth());

  // if credentials are not passed, return unlogged client
  if (!user) return feathersClient;

  // create user
  const usersService = app.service('users');
  const createdUser = await usersService.create({ ...user });

  global.users.push(createdUser);

  // authenticate and return
  return feathersClient
    .authenticate({
      strategy: 'local',
      email: user.email,
      password: user.password
    })
    .then((response) => {

      // Add Axios client for REST requests
      const restClient = axios.create({
        baseURL: `http://localhost:${port}`,
        headers: {'Authorization': response.accessToken}
      });
      feathersClient.set('restClient', restClient);

      return feathersClient.passport.verifyJWT(response.accessToken);
    })
    .then(payload => {
      return feathersClient.service('users').get(payload.userId);
    })
    .then(user => {
      feathersClient.set('user', user);
      return feathersClient;
    });
};
