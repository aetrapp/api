const app = require("../src/app");

// Config
const config = require("config");
const port = config.get("port");

describe("Application tests", () => {
  before(function(done) {
    this.server = app.listen(port);
    this.server.once("listening", async () => {
      global.app = app;
      global.users = [];

      // Clear database
      const sequelizeClient = app.get("sequelizeClient");
      await sequelizeClient.query('DELETE FROM "citiesModerators";');
      await sequelizeClient.query("DELETE FROM users;");
      await sequelizeClient.query("DELETE FROM traps;");
      await sequelizeClient.query("DELETE FROM samples;");
      await sequelizeClient.query("DELETE FROM notifications;");
      await sequelizeClient.query("DELETE FROM broadcasts;");
      done();
    });
  });

  /*
   * Test files are required as bellow to make sure they are executed
   * in the exact order they are declared.
   */
  require("./test-public.js");
  require("./test-users.js");
  require("./test-traps.js");
  require("./test-samples.js");
  require("./test-city-average-count.js");
  require("./test-samples-cycle-flow.js");
  require("./test-broadcasts.js");

});
