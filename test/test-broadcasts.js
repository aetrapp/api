const _ = require("lodash");
const moment = require("moment");
const should = require("should");

// Local variables
let allUsersIds, regularUsersIds, validUserIds, broadcast1, broadcast2;
const city1Id = "1100015";
const city2Id = "1100049";

// Helpers
const UserClientFactory = require("./helpers/user-client-factory");

// Fixtures
const trapBase64Image =
  "data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAAQAAAAECAYAAACp8Z5+AAAKumlDQ1BJQ0MgUHJvZmlsZQAASImVlgdYE9kWgO/MpBdaAAEpoXekE0BK6KEI0kFUQhIglBgTgorYWVyBFUVFBJUFXRRQcFWKLCpiwcIi2LCgG2RRUdfFgqiovAEecfe97733vTPfmft/Z84995w7937fAYDyO1sozIAVAMgUZInC/b3osXHxdPxjgAGagASwwJ7NEQuZYWHBAJXZ8e/y/g6ApsabllOx/v37fxVFLk/MAQAKQzmJK+ZkonwS1W6OUJQFALIeteuvyBJO8UGUlUVogii3TnHKDHdPcdIMS6d9IsO9UX4HAIHCZotSAKBMrUXP5qSgcSh0lK0FXL4A5al13TmpbC7K21C2yMxcNsXtKJsk/SVOyt9iJslistkpMp6pZVoIPnyxMIO96v/cjv8tmRmS2TX0UaWkigLC0VFtat/SlwXJWJC0IHSW+dxp/2lOlQREzTJH7B0/y1y2T5BsbsaC4FlO5vuxZHGyWJGzLFoWLovPE/tGzDJb9G0tSXoUU7YujyWLmZMaGTPL2fzoBbMsTo8I+ubjLbOLJOGynJNFfrIaM8V/qYvPkvlnpUYGyGpkf8uNJ46V5cDl+fjK7IIomY8wy0sWX5gRJvPnZfjL7OLsCNncLPSwfZsbJtufNHZg2CwDH+ALgtGHDiKALbBDH2sQAmKzeCuzpgrwXiZcJeKnpGbRmegN4tFZAo6VBd3W2oYBwNR9nPndb+9O3zNIlfDNlonuC8MSPZeN32yJNQA02QKgkPjNZjIAgFIVAGe1ORJR9owNM/XCordcHigDdaCNnicTYInm5whcgSeacSAIBZEgDiwBHJAKMoEIrAC5YAPIB4VgG9gFykElOAAOg6PgOGgB7eAcuASugV5wGzwAUjAMXoBR8B5MQBCEh6gQDVKHdCBDyByyhRiQO+QLBUPhUByUCKVAAkgC5UKboEKoBCqHqqBa6GfoFHQOugL1QfegQWgEegN9ghGYAivDWrARPA9mwEw4CI6EF8Mp8HI4B86Dt8JlcDV8BG6Gz8HX4NuwFH4BjyEAISOqiC5iiTAQbyQUiUeSERGyFilASpFqpAFpQ7qQm4gUeYl8xOAwNAwdY4lxxQRgojAczHLMWkwRphxzGNOMuYC5iRnEjGK+YqlYTaw51gXLwsZiU7ArsPnYUmwNtgl7EXsbO4x9j8PhVHHGOCdcAC4Ol4ZbjSvC7cM14jpwfbgh3Bgej1fHm+Pd8KF4Nj4Ln4/fgz+CP4u/gR/GfyCQCToEW4IfIZ4gIGwklBLqCGcINwhPCRNEBaIh0YUYSuQSVxGLiQeJbcTrxGHiBEmRZExyI0WS0kgbSGWkBtJF0gDpLZlM1iM7kxeS+eT15DLyMfJl8iD5I0WJYkbxpiRQJJStlEOUDso9ylsqlWpE9aTGU7OoW6m11PPUR9QPcjQ5KzmWHFdunVyFXLPcDblX8kR5Q3mm/BL5HPlS+RPy1+VfKhAVjBS8FdgKaxUqFE4p9CuMKdIUbRRDFTMVixTrFK8oPlPCKxkp+SpxlfKUDiidVxqiITR9mjeNQ9tEO0i7SBtWxikbK7OU05QLlY8q9yiPqiip2KtEq6xUqVA5rSJVRVSNVFmqGarFqsdV76h+mqM1hzmHN2fLnIY5N+aMq81V81TjqRWoNardVvukTlf3VU9X367eov5QA6NhprFQY4XGfo2LGi/nKs91ncuZWzD3+Nz7mrCmmWa45mrNA5rdmmNa2lr+WkKtPVrntV5qq2p7aqdp79Q+oz2iQ9Nx1+Hr7NQ5q/OcrkJn0jPoZfQL9FFdTd0AXYlulW6P7oSesV6U3ka9Rr2H+iR9hn6y/k79Tv1RAx2DEINcg3qD+4ZEQ4ZhquFuwy7DcSNjoxijzUYtRs+M1YxZxjnG9cYDJlQTD5PlJtUmt0xxpgzTdNN9pr1msJmDWapZhdl1c9jc0Zxvvs+8zwJr4WwhsKi26LekWDItsy3rLQetVK2CrTZatVi9mmcwL37e9nld875aO1hnWB+0fmCjZBNos9GmzeaNrZktx7bC9pYd1c7Pbp1dq91re3N7nv1++7sONIcQh80OnQ5fHJ0cRY4NjiNOBk6JTnud+hnKjDBGEeOyM9bZy3mdc7vzRxdHlyyX4y5/ulq6prvWuT6bbzyfN//g/CE3PTe2W5Wb1J3unuj+o7vUQ9eD7VHt8dhT35PrWeP5lGnKTGMeYb7ysvYSeTV5jXu7eK/x7vBBfPx9Cnx6fJV8o3zLfR/56fml+NX7jfo7+K/27wjABgQFbA/oZ2mxOKxa1migU+CawAtBlKCIoPKgx8FmwaLgthA4JDBkR8jAAsMFggUtoSCUFboj9GGYcdjysF8W4haGLaxY+CTcJjw3vCuCFrE0oi7ifaRXZHHkgyiTKElUZ7R8dEJ0bfR4jE9MSYw0dl7smthrcRpx/LjWeHx8dHxN/Ngi30W7Fg0nOCTkJ9xZbLx45eIrSzSWZCw5vVR+KXvpiURsYkxiXeJndii7mj2WxEramzTK8ebs5rzgenJ3ckd4brwS3tNkt+SS5Gcpbik7UkZSPVJLU1/yvfnl/NdpAWmVaePpoemH0iczYjIaMwmZiZmnBEqCdMGFZdrLVi7rE5oL84XS5S7Ldy0fFQWJasSQeLG4NUsZbXy6JSaS7ySD2e7ZFdkfVkSvOLFScaVgZfcqs1VbVj3N8cv5aTVmNWd1Z65u7obcwTXMNVVrobVJazvX6a/LWze83n/94Q2kDekbft1ovbFk47tNMZva8rTy1ucNfef/XX2+XL4ov3+z6+bK7zHf87/v2WK3Zc+WrwXcgquF1oWlhZ+LOEVXf7D5oeyHya3JW3uKHYv3b8NtE2y7s91j++ESxZKckqEdITuad9J3Fux8t2vpriul9qWVu0m7JbulZcFlrXsM9mzb87k8tfx2hVdF417NvVv2ju/j7rux33N/Q6VWZWHlpx/5P96t8q9qrjaqLj2AO5B94MnB6INdPzF+qq3RqCms+XJIcEh6OPzwhVqn2to6zbrierheUj9yJOFI71Gfo60Nlg1VjaqNhcfAMcmx5z8n/nzneNDxzhOMEw0nDU/ubaI1FTRDzauaR1tSW6Stca19pwJPdba5tjX9YvXLoXbd9orTKqeLz5DO5J2ZPJtzdqxD2PHyXMq5oc6lnQ/Ox56/dWHhhZ6LQRcvX/K7dL6L2XX2stvl9isuV05dZVxtueZ4rbnbobvpV4dfm3oce5qvO11v7XXubeub33fmhseNczd9bl66xbp17faC2313ou7c7U/ol97l3n12L+Pe6/vZ9ycerB/ADhQ8VHhY+kjzUfVvpr81Sh2lpwd9BrsfRzx+MMQZevG7+PfPw3lPqE9Kn+o8rX1m+6x9xG+k9/mi58MvhC8mXub/ofjH3lcmr07+6fln92js6PBr0evJN0Vv1d8eemf/rnMsbOzR+8z3E+MFH9Q/HP7I+Nj1KebT04kVn/Gfy76Yfmn7GvR1YDJzclLIFrGnWwEEVTg5GYA3hwCgxgFA6wWAtGimX54WaKbHnybwn3imp54WRwAOeAIQ2QFAKDpWoqMxqorop7ApuyeA7exk+k8RJ9vZzsQit6CtSenk5Fu0T8SbAvClf3JyomVy8gva2yD3Aeh4P9OnT4nCEQB6A22YcZFX7eVywb/IPwAu/ghox3qBGQAAAZlpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IlhNUCBDb3JlIDUuNC4wIj4KICAgPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4KICAgICAgPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIKICAgICAgICAgICAgeG1sbnM6ZXhpZj0iaHR0cDovL25zLmFkb2JlLmNvbS9leGlmLzEuMC8iPgogICAgICAgICA8ZXhpZjpQaXhlbFhEaW1lbnNpb24+NDwvZXhpZjpQaXhlbFhEaW1lbnNpb24+CiAgICAgICAgIDxleGlmOlBpeGVsWURpbWVuc2lvbj40PC9leGlmOlBpeGVsWURpbWVuc2lvbj4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+Cpcz29QAAAAVSURBVAgdY/wPBAxIgAmJDWYSFgAABl0EBCOMbBwAAAAASUVORK5CYII=";

describe("Broadcasts service", () => {
  before(async () => {
    // Clear relevant data from database
    const sequelizeClient = global.app.get("sequelizeClient");
    await sequelizeClient.query('DELETE FROM "citiesModerators";');
    await sequelizeClient.query("DELETE FROM traps;");
    await sequelizeClient.query("DELETE FROM notifications;");
    await sequelizeClient.query("DELETE FROM broadcasts;");

    // Add new regular user
    global.regularUser2 = await UserClientFactory(global.app, {
      firstName: "User",
      lastName: "1",
      email: "user2@aetrapp.org",
      password: "123456"
    }).catch(err => {
      should.not.exist(err);
    });

    // Get all user ids to use later
    allUsersIds = [
      global.moderatorUser1.get("user").id,
      global.adminUser1.get("user").id,
      global.regularUser1.get("user").id,
      global.regularUser2.get("user").id
    ];

    // Get all user ids to use later
    regularUsersIds = [
      global.regularUser1.get("user").id,
      global.regularUser2.get("user").id
    ];

    // Create active trap for user 1 in city 1
    let res = await global.regularUser1
      .service("traps")
      .create({
        addressStreet: "User1 City1 Active",
        cityId: city1Id,
        base64: trapBase64Image,
        coordinates: {
          type: "Point",
          coordinates: [-46.6547, -23.5639],
          crs: {
            type: "name",
            properties: {
              name: "EPSG:4326"
            }
          }
        }
      })
      .catch(err => {
        should.not.exist(err);
      });
    should.exist(res);

    // Create inactive traps for moderator 1 in city 1
    res = await global.moderatorUser1
      .service("traps")
      .create({
        addressStreet: "Moderator1 City1 Non-Active",
        cityId: city1Id,
        base64: trapBase64Image,
        isActive: false,
        coordinates: {
          type: "Point",
          coordinates: [-46.6547, -23.5639],
          crs: {
            type: "name",
            properties: {
              name: "EPSG:4326"
            }
          }
        }
      })
      .catch(err => {
        should.not.exist(err);
      });
    should.exist(res);

    // Create active trap for user 2 in city 2
    res = await global.regularUser2
      .service("traps")
      .create({
        addressStreet: "User2 City2 Active",
        cityId: city2Id,
        base64: trapBase64Image,
        coordinates: {
          type: "Point",
          coordinates: [-46.6547, -23.5639],
          crs: {
            type: "name",
            properties: {
              name: "EPSG:4326"
            }
          }
        }
      })
      .catch(err => {
        should.not.exist(err);
      });
    should.exist(res);
  });

  it("Unlogged user cannot access the service", async () => {
    await global.unloggedUser
      .service("broadcasts")
      .find({})
      .catch(err => {
        should.exist(err);
        should(err.code).be.equal(401);
      });

    await global.unloggedUser
      .service("broadcasts")
      .get("id")
      .catch(err => {
        should.exist(err);
        should(err.code).be.equal(401);
      });
  });

  it('Broadcast type must be "global", "city" or "direct"', async () => {
    validUserIds = [
      global.moderatorUser1.get("user").id,
      global.adminUser1.get("user").id,
      global.regularUser1.get("user").id
    ];

    let res = await global.adminUser1
      .service("broadcasts")
      .create({
        title: "Broadcast1",
        message: "broadcast",
        type: "invalid",
        recipients: {
          users: validUserIds
        }
      })
      .catch(err => {
        should.exist(err);
      });
    should.not.exist(res);
  });

  it("Only admins can create global broadcasts", async () => {
    await global.adminUser1
      .service("broadcasts")
      .find({})
      .catch(err => {
        should.not.exist(err);
      });

    let res = await global.regularUser1
      .service("broadcasts")
      .create({
        title: "Broadcast1",
        message: "broadcast",
        type: "global"
      })
      .catch(err => {
        should.exist(err);
      });
    should.not.exist(res);

    res = await global.moderatorUser1
      .service("broadcasts")
      .create({
        title: "Broadcast1",
        message: "broadcast",
        type: "global"
      })
      .catch(err => {
        should.exist(err);
      });
    should.not.exist(res);

    broadcast1 = await global.adminUser1
      .service("broadcasts")
      .create({
        title: "Broadcast1",
        message: "broadcast",
        type: "global"
      })
      .catch(err => {
        should.not.exist(err);
      });
    should.exist(broadcast1);

    // Get all notifications to check if all users got one
    const serverNotificationService = global.app.services["notifications"];
    const notifications = await serverNotificationService.find({
      query: {
        type: "global"
      },
      paginate: false
    });
    should.exist(notifications);
    should(notifications).have.length(4);

    // Map notification ids
    const notificationIds = _.uniq(_.map(notifications, n => n.recipientId));
    should(notificationIds).have.length(4);

    // All user ids must be in array
    for (const userId of allUsersIds) {
      should(notificationIds.indexOf(userId) == -1).be.false();
    }
  });

  it("City broadcasts must have specific properties", async () => {
    const malformedRecipientPropertyErrorString =
      'Missing or mal-formed "recipient.cities" property, it must be an Array of valid city ids.';
    const citiesNotFoundErrorString =
      'City ids of array in "recipient.cities" were not found.';

    // missing recipients cities
    broadcast2 = await global.adminUser1
      .service("broadcasts")
      .create({
        title: "Broadcast1",
        message: "broadcast",
        type: "city"
      })
      .catch(err => {
        should.exist(err);
        should(err.code).be.equal(400);
        should(err.message).be.equal(malformedRecipientPropertyErrorString);
      });
    should.not.exist(broadcast2);

    // empty city ids should raise error
    broadcast2 = await global.adminUser1
      .service("broadcasts")
      .create({
        title: "Broadcast1",
        message: "broadcast",
        type: "city",
        recipients: {
          cities: []
        }
      })
      .catch(err => {
        should.exist(err);
        should(err.code).be.equal(400);
        should(err.message).be.equal(malformedRecipientPropertyErrorString);
      });
    should.not.exist(broadcast2);

    // invalid city ids should raise error
    broadcast2 = await global.adminUser1
      .service("broadcasts")
      .create({
        title: "Broadcast1",
        message: "broadcast",
        type: "city",
        recipients: {
          cities: [city2Id, 7777777]
        }
      })
      .catch(err => {
        should.exist(err);
        should(err.code).be.equal(400);
        should(err.message).be.equal(citiesNotFoundErrorString);
      });
    should.not.exist(broadcast2);

    // valid city ids should not raise error
    broadcast2 = await global.adminUser1
      .service("broadcasts")
      .create({
        title: "Broadcast1",
        message: "broadcast",
        type: "city",
        recipients: {
          cities: [city2Id, 1100031]
        }
      })
      .catch(err => {
        should.not.exist(err);
      });
    should.exist(broadcast2);

    // Only user1 should recieve notification
    const serverNotificationService = global.app.services["notifications"];
    const notifications = await serverNotificationService.find({
      query: {
        broadcastId: broadcast2.id
      },
      paginate: false
    });
    should.exist(notifications);
    should(notifications).have.length(1);
    should(notifications[0].recipientId).be.equal(
      global.regularUser2.get("user").id
    );
  });

  it("Moderators can create city broadcasts for their cities", async () => {
    const restClient = global.adminUser1.get("restClient");
    await restClient.post(
      `/users/${global.moderatorUser1.get("user").id}/allowedCities`,
      {
        cityId: city1Id
      }
    );
    await restClient.post(
      `/users/${global.moderatorUser1.get("user").id}/allowedCities`,
      {
        cityId: city2Id
      }
    );

    // Broadcast to one allowed city
    let res = await global.moderatorUser1
      .service("broadcasts")
      .create({
        title: "Broadcast1",
        message: "broadcast",
        type: "city",
        recipients: {
          cities: [city1Id]
        }
      })
      .catch(err => {
        should.not.exist(err);
      });
    should.exist(res);

    // Broadcast to two allowed cities
    res = await global.moderatorUser1
      .service("broadcasts")
      .create({
        title: "Broadcast1",
        message: "broadcast",
        type: "city",
        recipients: {
          cities: [city1Id, city2Id]
        }
      })
      .catch(err => {
        should.not.exist(err);
      });
    should.exist(res);

    // Try to broadcast to unallowed city
    res = await global.moderatorUser1
      .service("broadcasts")
      .create({
        title: "Broadcast1",
        message: "broadcast",
        type: "city",
        recipients: {
          cities: [1100031, city2Id]
        }
      })
      .catch(err => {
        should.exist(err);
      });
    should.not.exist(res);
  });

  it('"global" broadcast add notifications for all users', async () => {
    const broadcast1Data = {
      title: "GLOBAL",
      message: "A global broadcast to test notifications",
      type: "global"
    };

    const res = await global.adminUser1
      .service("broadcasts")
      .create(broadcast1Data)
      .catch(err => {
        should.not.exist(err);
      });
    should.exist(res);

    const serverNotificationService = global.app.services["notifications"];
    const notifications = await serverNotificationService.find({
      query: {
        broadcastId: res.id
        // $limit: 1
      }
    });

    const serverUsersService = global.app.services["users"];
    const users = await serverUsersService.find({
      query: {
        $limit: 0
      }
    });

    should(notifications).have.property("total", users.total);
    should(notifications.total).be.greaterThan(0);
    should(notifications.data[0]).have.property("type", broadcast1Data.type);
    should(notifications.data[0]).have.property("title", broadcast1Data.title);
    should(notifications.data[0]).have.property(
      "message",
      broadcast1Data.message
    );
    should(notifications.data[0]).have.property("broadcastId", res.id);
  });

  it('"city" broadcast generate notifications to users with active traps in cities', async () => {
    const broadcastData = {
      title: "Test city notification",
      message: "Notification to the city",
      type: "city",
      recipients: {
        cities: [city1Id, city2Id]
      }
    };

    const broadcast = await global.adminUser1
      .service("broadcasts")
      .create(broadcastData)
      .catch(err => {
        should.not.exist(err);
      });
    should.exist(broadcast);

    const serverNotificationsService = global.app.services["notifications"];
    const notifications = await serverNotificationsService.find({
      query: {
        broadcastId: broadcast.id
      }
    });

    should(notifications).have.property("total", 2);
    should(notifications.data[0]).have.property("type", broadcastData.type);
    should(notifications.data[0]).have.property("title", broadcastData.title);
    should(notifications.data[0]).have.property(
      "message",
      broadcastData.message
    );
    should(notifications.data[0]).have.property("broadcastId", broadcast.id);

    const notificationIds = _.uniq(
      _.map(notifications.data, n => n.recipientId)
    );

    for (const userId of regularUsersIds) {
      should(notificationIds.indexOf(userId) > -1).be.true();
    }
  });

  it("Only admins can broadcast to users", async () => {
    const directBroadcastData = {
      title: "DIRECT",
      message: "Direct notification to user",
      type: "direct",
      deliveryTime: "2020-01-20T03:33:33Z",
      recipients: {
        users: regularUsersIds
      }
    };

    // Create broadcast with admin
    let broadcast = await global.adminUser1
      .service("broadcasts")
      .create(directBroadcastData)
      .catch(err => {
        should.not.exist(err);
      });
    should.exist(broadcast);

    // Verify notification creation
    const serverNotificationsService = global.app.services["notifications"];
    const notifications = await serverNotificationsService.find({
      query: {
        broadcastId: broadcast.id
      }
    });

    should(notifications).have.property("total", 2);
    should(notifications.data[0]).have.property(
      "type",
      directBroadcastData.type
    );
    should(notifications.data[0]).have.property(
      "title",
      directBroadcastData.title
    );
    should(notifications.data[0]).have.property(
      "message",
      directBroadcastData.message
    );
    should(moment(notifications.data[0].deliveryTime).valueOf()).be.equal(
      moment(directBroadcastData.deliveryTime).valueOf()
    );
    should(notifications.data[0]).have.property("broadcastId", broadcast.id);

    // Regular users must be recipients
    for (const notification of notifications.data) {
      should(regularUsersIds.indexOf(notification.recipientId) > -1).be.true();
    }

    let res = await global.moderatorUser1
      .service("broadcasts")
      .create(directBroadcastData)
      .catch(err => {
        should.exist(err);
      });
    should.not.exist(res);

    res = await global.regularUser1
      .service("broadcasts")
      .create(directBroadcastData)
      .catch(err => {
        should.exist(err);
      });
    should.not.exist(res);
  });

  it("Admin can access all messages");

  it("Moderators can only access own and city broadcasts");

  it("Disable update/patch");

  it("Moderators can remove own messages only");

  it("Admins can remove any broadcast");
});
