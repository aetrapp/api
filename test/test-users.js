const should = require("should");

// Helpers
const UserClientFactory = require("./helpers/user-client-factory");

describe("Users service", () => {
  it("First user is admin", async () => {
    const serverUsersService = global.app.services["users"];

    global.adminUser1 = await UserClientFactory(global.app, {
      firstName: "Admin",
      lastName: "1",
      email: "admin@aetrapp.org",
      password: "123456"
    }).catch(err => {
      should.not.exist(err);
    });

    const admin1 = await serverUsersService.get(
      global.adminUser1.get("user").id
    );

    admin1.should.have.property("roles");
    admin1.roles.should.be.an.Array().and.containEql("admin");
  });

  it("Subsequent users should not be admin and not allowed to set roles property", async () => {
    // create unlogged user for public tests
    global.unloggedUser = await UserClientFactory(global.app).catch(err => {
      should.not.exist(err);
    });

    // create regular to be used in tests
    global.regularUser1 = await UserClientFactory(global.app, {
      firstName: "User",
      lastName: "1",
      email: "user1@aetrapp.org",
      password: "123456",
      roles: ["admin"]
    }).catch(err => {
      should.not.exist(err);
    });

    const serverUsersService = global.app.services["users"];

    const serverRegularUser1 = await serverUsersService.get(
      global.regularUser1.get("user").id
    );
    serverRegularUser1.should.have.property("roles");
    serverRegularUser1.roles.should.be.an.Array().of.length(0);
  });

  it("Admin can change roles", async () => {
    const clientUsersService = global.adminUser1.service("users");
    const serverUsersService = global.app.services["users"];

    await clientUsersService.patch(global.regularUser1.get("user").id, {
      roles: ["admin"]
    });
    let serverRegularUser1 = await serverUsersService.get(
      global.regularUser1.get("user").id
    );

    serverRegularUser1.should.have.property("roles");
    serverRegularUser1.roles.should.be.an
      .Array()
      .containEql("admin")
      .of.length(1);

    await clientUsersService.patch(global.regularUser1.get("user").id, {
      roles: ["moderator"]
    });
    serverRegularUser1 = await serverUsersService.get(
      global.regularUser1.get("user").id
    );

    serverRegularUser1.should.have.property("roles");
    serverRegularUser1.roles.should.be.an
      .Array()
      .containEql("moderator")
      .of.length(1);

    await clientUsersService.patch(global.regularUser1.get("user").id, {
      roles: []
    });
    serverRegularUser1 = await serverUsersService.get(
      global.regularUser1.get("user").id
    );

    serverRegularUser1.should.have.property("roles");
    serverRegularUser1.roles.should.be.an.Array().of.length(0);

    // create moderator to be used in tests
    global.moderatorUser1 = await UserClientFactory(global.app, {
      firstName: "Moderator",
      lastName: "1",
      email: "moderator1@aetrapp.org",
      password: "123456",
      roles: ["moderator"]
    }).catch(err => {
      should.not.exist(err);
    });

    let serverModeratorUser1 = await serverUsersService.get(
      global.regularUser1.get("user").id
    );
    serverModeratorUser1.should.have.property("roles");
    serverModeratorUser1.roles.should.be.an.Array().of.length(0);

    await clientUsersService.patch(global.moderatorUser1.get("user").id, {
      roles: ["moderator"]
    });
    serverModeratorUser1 = await serverUsersService.get(
      global.moderatorUser1.get("user").id
    );

    serverModeratorUser1.should.have.property("roles");
    serverModeratorUser1.roles.should.be.an
      .Array()
      .containEql("moderator")
      .of.length(1);
  });

  it("Moderators cannot change roles for himself or others", async () => {
    const clientUsersService = global.moderatorUser1.service("users");
    const serverUsersService = global.app.services["users"];

    // check own role change
    let res = await clientUsersService
      .patch(global.moderatorUser1.get("user").id, {
        roles: ["admin"]
      })
      .catch(err => {
        should.exist(err);
        should(err.code).be.equal(403);
        should(err.message).be.equal(
          "You do not have valid permissions to access this."
        );
      });
    should.not.exist(res);

    let serverUser = await serverUsersService.get(
      global.moderatorUser1.get("user").id
    );
    serverUser.should.have.property("roles");
    serverUser.roles.should.be.an
      .Array()
      .containEql("moderator")
      .of.length(1);

    // check other role change
    res = await clientUsersService
      .patch(global.regularUser1.get("user").id, {
        roles: ["admin"]
      })
      .catch(err => {
        should.exist(err);
        should(err.code).be.equal(403);
        should(err.message).be.equal(
          "You do not have the permissions to access this."
        );
      });
    should.not.exist(res);

    serverUser = await serverUsersService.get(
      global.regularUser1.get("user").id
    );
    serverUser.should.have.property("roles");
    serverUser.roles.should.be.an.Array().of.length(0);
  });

  it("Regular users cannot change roles", async () => {
    const clientUsersService = global.regularUser1.service("users");
    const serverUsersService = global.app.services["users"];

    // check own role change
    let res = await clientUsersService
      .patch(global.regularUser1.get("user").id, {
        roles: ["admin"]
      })
      .catch(err => {
        should.exist(err);
        should(err.code).be.equal(403);
        should(err.message).be.equal(
          "You do not have valid permissions to access this."
        );
      });
    should.not.exist(res);

    let serverUser = await serverUsersService.get(
      global.regularUser1.get("user").id
    );
    serverUser.should.have.property("roles");
    serverUser.roles.should.be.an.Array().of.length(0);

    // check other role change
    res = await clientUsersService
      .patch(global.moderatorUser1.get("user").id, {
        roles: ["admin"]
      })
      .catch(err => {
        should.exist(err);
        should(err.code).be.equal(403);
        should(err.message).be.equal(
          "You do not have the permissions to access this."
        );
      });
    should.not.exist(res);

    serverUser = await serverUsersService.get(
      global.moderatorUser1.get("user").id
    );
    serverUser.should.have.property("roles");
    serverUser.roles.should.be.an
      .Array()
      .containEql("moderator")
      .of.length(1);
  });

  it("Admin can create city and moderator relation", async () => {
    const restClient = global.adminUser1.get("restClient");
    const res = await restClient.post(
      `/users/${global.moderatorUser1.get("user").id}/allowedCities`,
      {
        cityId: 1100015
      }
    );
    should.exist(res);
    should(res).have.property("status", 201);
  });

  it("Moderator cannot create city and moderator relation", async () => {
    const restClient = global.moderatorUser1.get("restClient");
    const res = await restClient
      .post(`/users/${global.moderatorUser1.get("user").id}/allowedCities`, {
        cityId: 1100015
      })
      .catch(err => {
        should.exist(err);
        should(err.response.status).be.equal(403);
      });
    should.not.exist(res);
  });

  it("Regular user cannot change relation to city", async () => {
    const restClient = global.regularUser1.get("restClient");
    const res = await restClient
      .post(`/users/${global.regularUser1.get("user").id}/allowedCities`, {
        cityId: 1100015
      })
      .catch(err => {
        should.exist(err);
        should(err.response.status).be.equal(403);
      });
    should.not.exist(res);
  });

  it("Only admins and moderators can get cities moderated by an user", async () => {
    const cityId = 1100015;
    let restClient = global.adminUser1.get("restClient");
    let res = await restClient
      .get(`/users/${global.moderatorUser1.get("user").id}/allowedCities`)
      .catch(err => {
        should.not.exist(err);
      });
    should(res).have.property("status", 200);
    should.exist(res.data);
    should(res.data[0].id).be.equal(cityId.toString());

    restClient = global.moderatorUser1.get("restClient");
    res = await restClient
      .get(`/users/${global.moderatorUser1.get("user").id}/allowedCities`)
      .catch(err => {
        should.not.exist(err);
      });
    should(res).have.property("status", 200);
    should.exist(res.data);
    should(res.data[0].id).be.equal(cityId.toString());

    restClient = global.regularUser1.get("restClient");
    res = await restClient
      .get(`/users/${global.moderatorUser1.get("user").id}/allowedCities`)
      .catch(err => {
        should.exist(err);
        should(err.response.status).be.equal(403);
      });
    should.not.exist(res);
  });

  it("Only admin can remove city and moderator relation", async () => {
    const cityId = 1100015;
    let restClient = global.regularUser1.get("restClient");
    let res = await restClient
      .delete(
        `/users/${global.moderatorUser1.get("user").id}/allowedCities/${cityId}`
      )
      .catch(err => {
        should.exist(err);
        should(err.response.status).be.equal(403);
      });
    should.not.exist(res);

    restClient = global.moderatorUser1.get("restClient");
    res = await restClient
      .delete(
        `/users/${global.moderatorUser1.get("user").id}/allowedCities/${cityId}`
      )
      .catch(err => {
        should.exist(err);
        should(err.response.status).be.equal(403);
      });
    should.not.exist(res);

    restClient = global.adminUser1.get("restClient");
    res = await restClient
      .delete(
        `/users/${global.moderatorUser1.get("user").id}/allowedCities/${cityId}`
      )
      .catch(err => {
        should.not.exist(err);
      });
    should.exist(res);
    should(res).have.property("status", 200);
  });
});
