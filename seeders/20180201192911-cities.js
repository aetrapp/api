"use strict";

module.exports = {
  up: queryInterface => {
    var cities = require("../collections/cities.json");
    return queryInterface.bulkInsert("cities", cities, {});
  },

  down: queryInterface => {
    return queryInterface.bulkDelete("cities", null, {});
  }
};
